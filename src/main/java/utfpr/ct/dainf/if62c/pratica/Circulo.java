
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author loja
 */
public class Circulo extends Elipse{
    
    public double raio;

    public Circulo(double raio, double eixoMaior, double EixoMenor) {
        super(eixoMaior, EixoMenor);
        this.raio = raio;
    }
    public Circulo(double raio){
        this.raio=raio;
    }
    
    @Override
       public double getArea() {
        double areaCirculo;
        areaCirculo = Math.PI * raio * raio;
        return areaCirculo;
    }

    @Override
    public double getPerimetro() {
        double periCirculo;
        periCirculo = 2 * Math.PI * raio;
        return periCirculo;
    }
    
}
