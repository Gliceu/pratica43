
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gliceu
 */
public interface FiguraComLados {
    double getLadoMenor();
    double getLadoMaior();
//    double getArea();
//    double getPerimetro();
    
}
