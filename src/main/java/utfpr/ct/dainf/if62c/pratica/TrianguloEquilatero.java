
package utfpr.ct.dainf.if62c.pratica;
/**
 * @author Gliceu
 */
public class TrianguloEquilatero extends Retangulo { 
     public TrianguloEquilatero(double lado) {
        super(lado, lado * Math.sqrt(3) / 4);
    }    
    @Override
    public double getPerimetro (){
        return getLadoMaior()*3;
    }
}
