
package utfpr.ct.dainf.if62c.pratica;
/**
 * @author Gliceu
 */
public class Quadrado extends Retangulo{  
    public Quadrado(double lado){
        super(lado,lado);
    }   
}
