
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gliceu Camargo
 */
public interface FiguraComEixos extends Figura{
     public double getEixoMenor();
     public double getEixoMaior();
    
}
