
package utfpr.ct.dainf.if62c.pratica;
/**
 * @author Gliceu
 */
public class Retangulo implements FiguraComLados {
    public double ladoMaior;
    public double ladoMenor;

    public Retangulo(double ladoMaior, double ladoMenor) {
        this.ladoMaior = ladoMaior;
        this.ladoMenor = ladoMenor;
    }

    @Override
    public double getLadoMenor() {
        return ladoMenor;
    }

    @Override
    public double getLadoMaior() {
        return ladoMaior;
    }
    
    public double getArea(){
        double areaRetangulo;
        areaRetangulo= getLadoMaior()*getLadoMenor();
        return areaRetangulo;
    }
    public double getPerimetro(){
        double perRetangulo;
        perRetangulo = 2*(getLadoMaior() + getLadoMenor());
        return perRetangulo;     
    }
    
}
